//
//  ListController.swift
//  simple todo app
//
//  Created by Rhodry Cann on 13/06/2015.
//  Copyright (c) 2015 Rhodry Cann. All rights reserved.
//

import UIKit

class ListController: UITableViewController {
    
    var items:[String] = ["Bread", "Butter"]
    var savedList:[String] = []
    var savedItems:NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    
    @IBOutlet var newItem: UITextField!
    
    //functionality for 'add' bar button in nav bar and alerts it produces for the user
    @IBAction func showDialog(sender: UIBarButtonItem) {
        NSLog("showDialog")
        var alert:UIAlertController
        alert = UIAlertController(title: "New Item", message: "Type item below", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "add", style: UIAlertActionStyle.Default, handler: itemAdded))
        alert.addTextFieldWithConfigurationHandler(addTextField)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func addTextField(textField: UITextField!){
        NSLog("addTextField")
        self.newItem = textField
    }
    
    //function to log and append new items to array
    func itemAdded(alert: UIAlertAction!){
        
    //validation: Ignore blank strings
        let item:String = self.newItem.text
        let itemLength:Int = count(item)
        if itemLength > 0 {
            self.items.append(self.newItem.text)
        //To make data persistent
            self.savedItems.setObject(items, forKey: "savedList")
            self.savedItems.synchronize()
        }//end of validation
        
        NSLog("itemAdded")
        NSLog("item name: %@", self.newItem.text)
        NSLog("%@", self.items)
        
        //refresh page to display new items
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSLog("viewDidLoad")
        
        //Uncomment line to make data persistent and retrievable once the app is closed
        self.items = self.savedItems.arrayForKey("savedList") as! [String]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in table. size = aomunt of items in arraay
        return self.items.count
    }

    //function to add contents of array to table cells
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ShoppingItem", forIndexPath: indexPath) as! UITableViewCell
        cell.textLabel?.text = items[indexPath.row]
        return cell
    }
    //function to remove background of selected item and add/remove checkmarks to said item
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        NSLog("row %i", indexPath.row)
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let table:UITableView = self.tableView
        let cell:UITableViewCell = table.cellForRowAtIndexPath(indexPath)!
        if cell.accessoryType == UITableViewCellAccessoryType.Checkmark {
            cell.accessoryType = UITableViewCellAccessoryType.None
            
            
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
    }

    //function for deleting items
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            self.items.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
        NSLog("itemDeleted")
        NSLog("%@", self.items)
    }
    
    
    
    
    
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
